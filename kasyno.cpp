#include<iostream>
#include<cstdlib>
#include<chrono>
#include<thread>
#include<stdlib.h>
#include<sstream>
#include<limits>
#include<cstdio>
#include<algorithm>
#include<iterator>
#include<unistd.h>

void display();
void rules();
bool is_digits(const std::string &str);
void rollBlack(int tempo);
void rollRed(int tempo);
void rollGreen(int tempo);
char getch()
{
    system("stty raw");
    char c = getchar();
    system("stty cooked");
    return c;
}

int main()
{
    display();
    std::cout << "\n";
    int flag = 0;
	std::string name;
    char playAgain;
    std::cout<<"Wprowadź swoje imię: ";
    getline(std::cin, name);
    std::cout << std::endl;
    
	std::string strBalance;
	int balance;
    std::cout << "Wprowadź kwotę wejścia: $";
    std::cin >> strBalance;
    std::cout << "\n";
	    while(true)
		{
            if(is_digits(strBalance) == 0)
			{
                std::cout << "To nie jest poprawna kwota! Wprowadź ponownie: $";
                std::cin >> strBalance;
            }
            else 
			{
                balance = stoi(strBalance);
                break;
            }
        }

    do
    {
        rules();
        validate_bid:  
        std::string strBidAmount; // kwota obstawienia
		int bid_amount;
        std::cout << "Stan konta wynosi $" << balance << ".\n\n";
        std::cout << "Wprowadź kwotę jaką chcesz obstawić: $";
        std::cin >> strBidAmount; 
        std::cout<<"\n";
        while (true) // walidacja obstawienia
        {
            if(is_digits(strBidAmount) == 0)
            {
                std::cout << "To nie jest poprawna kwota! Wprowadź ponownie: ";
                std::cin >> strBidAmount;
            }
            else {
                bid_amount = stoi(strBidAmount); // konwertuje stringa na inta
                break;
            }
        }
        
        if(bid_amount <= balance)
        {
            goto roulette;
        }
        else if(bid_amount > balance)
        {
            std::cout << "Nie posiadasz wystarczającej ilości pieniędzy!" << std::endl;
            goto validate_bid;
        }

        roulette:
        int ball;
        srand(time(0)); //generuje inną liczbę z każdym uruchomieniem
        ball = rand()%37;
        int blackArray[] = {2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35};
        int redArray[] = {1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36};
        int greenArray[] = {0};

        bid_choice:
        
        char guess;
        std::cout << "Wciśnij 1 aby wybrać pole \u001b[30mCZARNE\u001b[0m"<<std::endl;
        std::cout<< "Wciśnij 2 aby wybrać pole \u001b[31mCZERWONE\u001b[0m "<<std::endl;
        std::cout << "Wciśnij 3 aby wybrać pole \u001b[32mZIELONE\u001b[0m"<<std::endl;
        std::cin.ignore();
        guess = getch();
        system("clear");
        if(guess == '1')
        {
            std::cout << "Wybrano kolor \u001b[30mCZARNY\u001b[0m. Losowanie..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(2));
            bool rngFinder = std::find(std::begin(blackArray), std::end(blackArray), ball) != std::end(blackArray); //sprawdza czy kula wylądowała w dowolnym z czarnych pól
            if (rngFinder == true)
            {
                flag = 1;
                rollBlack(0);
                rollRed(0);
                rollBlack(0);
                rollRed(1);
                rollBlack(1);
                rollRed(2);
                rollGreen(2);
                rollBlack(3);
                rollRed(4);
                rollBlack(5);
                rollRed(6);
                rollBlack(7);
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollBlack(0);
            }
            else
            {
                flag = 0;
                rollRed(0);
                rollBlack(0);
                rollRed(0);
                rollBlack(1);
                rollRed(1);
                rollBlack(2);
                rollRed(2);
                rollBlack(3);
                rollRed(3);
                rollBlack(4);
                rollRed(5);
                rollBlack(6);
                rollRed(7);
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollRed(0);
            }


        }
        else if(guess == '2')
        {
            std::cout << "Wybrano kolor \u001b[31mCZERWONY\u001b[0m. Losowanie..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(2));
            bool rngFinder = std::find(std::begin(redArray), std::end(redArray), ball) != std::end(redArray);
            if (rngFinder == true)
            {
                flag = 2;
                rollRed(0);
                rollBlack(0);
                rollRed(0);
                rollBlack(1);
                rollRed(1);
                rollBlack(2);
                rollRed(2);
                rollBlack(3);
                rollRed(3);
                rollBlack(4);
                rollRed(5);
                rollBlack(6);
                rollRed(7); 
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollRed(0);
            }
            else
            {
                flag = 0;
                rollBlack(0);
                rollRed(0);
                rollBlack(0);
                rollRed(1);
                rollGreen(1);
                rollBlack(2);
                rollRed(3);
                rollBlack(3);
                rollRed(4);
                rollBlack(5);
                rollRed(6);
                rollBlack(7);
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollBlack(0);
            }
        }
        else if(guess == '3')
        {
            std::cout << "Wybrano kolor \u001b[32mZIELONY\u001b[0m. Losowanie..." << std::endl;
            std::this_thread::sleep_for(std::chrono::seconds(2));
            bool rngFinder = std::find(std::begin(greenArray), std::end(greenArray), ball) != std::end(greenArray);
            if (rngFinder == true)
            {
                flag = 3;
                rollBlack(0);
                rollRed(0);
                rollBlack(0);
                rollRed(1);
                rollBlack(2);
                rollRed(2);
                rollBlack(3);
                rollRed(3);
                rollBlack(3);
                rollRed(4);
                rollBlack(5);
                rollRed(6);
                rollGreen(7);
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollGreen(0);

            }
            else
            {
                flag = 0;
                rollBlack(0);
                rollRed(0);
                rollBlack(0);
                rollRed(1);
                rollBlack(1);
                rollRed(2);
                rollBlack(2);
                rollRed(3);
                rollBlack(3);
                rollRed(4);
                rollGreen(5);
                rollBlack(7);
                std::this_thread::sleep_for(std::chrono::seconds(1));
                system("clear");
                rollBlack(0);
            }
        }
        else
        {
            system("clear");
            rules();
            std::cout << "\u001b[31mInvalid input!\u001b[0m"<<std::endl;
            goto bid_choice;
        }

        if(flag == 1) //flaga dla czarnego
        {

            std::cout<<"Gratulacje!"<<std::endl;
            std::cout<<"Wygrywasz $"<<bid_amount*2<<std::endl;
            balance=balance+bid_amount;
        }
          if(flag == 2) //flaga dla czerwonego
        {
            std::cout<<"Gratulacje!"<<std::endl;
            std::cout<<"Wygrywasz $"<<bid_amount*2<<std::endl;
            balance=balance+bid_amount;
        }
        else if(flag == 3) //flaga dla zielonego
        {
            std::cout << "Gratulacje! Wygrywasz!" << std::endl;
            std::cout << "Wygrywasz $" << bid_amount*35 << std::endl;
            balance = balance + bid_amount*34;
        }
        else if(flag == 0) //flaga dla przegranej
        {
            std::cout << "Przykro mi..." << std::endl;
            std::cout << "Przegrywasz $" << bid_amount << std::endl;
            balance = balance-bid_amount;
        }
        std::cout << "Chcesz zagrać ponownie? (T)ak/(N)ie: ";
        playAgain = getch();
        std::cout << std::endl << std::endl;
        if(balance == 0)
        {
            std::cout << "Nie masz wystarczających środków by grać dalej." << std::endl;
            goto exit;
        }
        if (playAgain == 't' || playAgain == 'T')
        {
            goto validate_bid;
        }
        else if (playAgain == 'n' || playAgain == 'N')
        {
            goto exit;
        }
    } 

    while(playAgain != 'n' || playAgain != 'N');
    exit:
    std::cout << std::endl;
    std::cout << name << ", " << "opuszczasz ruletkę z kwotą $" << balance << "." << std::endl;
    std::cout<<"\n\n***********************************************************************************"<<std::endl;
    return 0;
}
void display()
{

    std::string var="\n==================================================================================="
                    "\n\t __    __   ______    ______   __      __  __    __   ______ "
                    "\n\t/  |  /  | /      \\  /      \\ /  \\    /  |/  \\  /  | /      \\ "
                    "\n\t$$ | /$$/ /$$$$$$  |/$$$$$$  |$$  \\  /$$/ $$  \\ $$ |/$$$$$$  |"
                    "\n\t$$ |/$$/  $$ |__$$ |$$ \\__$$/  $$  \\/$$/  $$$  \\$$ |$$ |  $$ |"
                    "\n\t$$  $$<   $$    $$ |$$      \\   $$  $$/   $$$$  $$ |$$ |  $$ |"
                    "\n\t$$$$$  \\  $$$$$$$$ | $$$$$$  |   $$$$/    $$ $$ $$ |$$ |  $$ |"
                    "\n\t$$ |$$  \\ $$ |  $$ |/  \\__$$ |    $$ |    $$ |$$$$ |$$ \\__$$ |"
                    "\n\t$$ | $$  |$$ |  $$ |$$    $$/     $$ |    $$ | $$$ |$$    $$/ "
                    "\n\t$$/   $$/ $$/   $$/  $$$$$$/      $$/     $$/   $$/  $$$$$$/  \n"
                    "\n===================================================================================\n";
    for(int i=0;i<var.size();i++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        std::cout<<var[i];
    }
}
void rules()
{
    system("clear");
    std::cout<<"\n\n";
    std::cout<<"\n\n========================================================================================\n\n";
    std::cout<<"\t\t\t\t\tZASADY GRY\n";
    std::cout<<"\n========================================================================================\n\n";
    std::cout<<"\t1. Wybierz jedno z pól: \u001b[30mCZARNE\u001b[0m, \u001b[31mCZERWONE\u001b[0m lub  \u001b[32mZIELONE\u001b[0m.\n";
    std::cout<<"\t2. Trafne wybory dla pól \u001b[30mCZARNYCH\u001b[0m i \u001b[31mCZERWONYCH\u001b[0m podwajają obstawioną kwotę.\n";
    std::cout<<"\t3. Pole \u001b[32mZIELONE\u001b[0m mnoży obstawioną kwotę przez 35.\n";
    std::cout<<"\t4. Jeżeli wybór będzie nietrafiony stracisz obstawione pieniądze.\n";
    std::cout<<"\t5. Brak pieniędzy eliminuje cię z rozgrywki!\n";
    std::cout<<"\n========================================================================================\n\n";
}
bool is_digits(const std::string &str)
{
    return str.find_first_not_of("0123456789") == std::string::npos;
}
void rollBlack(int tempo)
{

std::string var="==================================================================================="
                    "\n\t\t\u001b[30m _______   __         ______    ______   __    __ "
                    "\n\t\t/       \\ /  |       /      \\  /      \\ /  |  /  |"
                    "\n\t\t$$$$$$$  |$$ |      /$$$$$$  |/$$$$$$  |$$ | /$$/ "
                    "\n\t\t$$ |__$$ |$$ |      $$ |__$$ |$$ |  $$/ $$ |/$$/  "
                    "\n\t\t$$    $$< $$ |      $$    $$ |$$ |      $$  $$<   "
                    "\n\t\t$$$$$$$  |$$ |      $$$$$$$$ |$$ |   __ $$$$$  \\  "
                    "\n\t\t$$ |__$$ |$$ |_____ $$ |  $$ |$$ \\__/  |$$ |$$  \\ "
                    "\n\t\t$$    $$/ $$       |$$ |  $$ |$$    $$/ $$ | $$  |"
                    "\n\t\t$$$$$$$/  $$$$$$$$/ $$/   $$/  $$$$$$/  $$/   $$/ \u001b[0m\n"
                    "\n===================================================================================\n";
    for(int i=0;i<var.size();i++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(tempo));
        std::cout<<var[i];
    }
}
void rollRed(int tempo)
{
  
    std::string var="==================================================================================="
                    "\n\t\t\t\u001b[31m _______   ________  _______  "
                    "\n\t\t\t/       \\ /        |/       \\ "
                    "\n\t\t\t$$$$$$$  |$$$$$$$$/ $$$$$$$  |"
                    "\n\t\t\t$$ |__$$ |$$ |__    $$ |  $$ |"
                    "\n\t\t\t$$    $$/ $$    |   $$ |  $$ |"
                    "\n\t\t\t$$$$$$$  |$$$$$/    $$ |  $$ |"
                    "\n\t\t\t$$ |  $$ |$$ |_____ $$ |__$$ |"
                    "\n\t\t\t$$ |  $$ |$$       |$$    $$/ " 
                    "\n\t\t\t$$/   $$/ $$$$$$$$/ $$$$$$$/  \u001b[0m\n"
                    "\n===================================================================================\n";
    for(int i=0;i<var.size();i++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(tempo));
        std::cout<<var[i];
    }  
}
void rollGreen(int tempo)
{
  
    std::string var="==================================================================================="
                    "\n\t\t\u001b[32m  ______   _______   ________  ________  __    __ "
                    "\n\t\t /      \\ /       \\ /        |/        |/  \\  /  |"
                    "\n\t\t/$$$$$$  |$$$$$$$  |$$$$$$$$/ $$$$$$$$/ $$  \\ $$ |"
                    "\n\t\t$$ | _$$/ $$ |__$$ |$$ |__    $$ |__    $$$  \\$$ |"
                    "\n\t\t$$ |/    |$$    $$/ $$    |   $$    |   $$$$  $$ |"
                    "\n\t\t$$ |$$$$ |$$$$$$$  |$$$$$/    $$$$$/    $$ $$ $$ |"
                    "\n\t\t$$ \\__$$ |$$ |  $$ |$$ |_____ $$ |_____ $$ |$$$$ |"
                    "\n\t\t$$    $$/ $$ |  $$ |$$       |$$       |$$ | $$$ |"
                    "\n\t\t $$$$$$/  $$/   $$/ $$$$$$$$/ $$$$$$$$/ $$/   $$/ \u001b[0m\n"
                    "\n===================================================================================\n";
    for(int i=0;i<var.size();i++)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(tempo));
        std::cout<<var[i];
    } 
}
